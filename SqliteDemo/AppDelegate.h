//
//  AppDelegate.h
//  SqliteDemo
//
//  Created by Redbytes on 11/01/16.
//  Copyright © 2016 Redbytes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

